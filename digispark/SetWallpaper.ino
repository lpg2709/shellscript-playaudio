/* 
 * File: SetWallpaper
 * Description: Set wallpaper on windows using a Digispark with Attiny85
 * This script was tested on Window 10 and 11;
 */

// For pt-BR keyboard
#include "DigiKeyboardAbnt2.h"

// For en-US Keyboard remove comment on line bellow
// #include "DigiKeyboard.h"

void sendText(const char *str) {
  DigiKeyboard.print(str);
  DigiKeyboard.sendKeyStroke(KEY_ENTER);
  DigiKeyboard.delay(500);
}

void setup() {}

void loop() {
  DigiKeyboard.sendKeyStroke(0);
  DigiKeyboard.sendKeyStroke(KEY_R, MOD_GUI_LEFT);
  DigiKeyboard.delay(500);
  sendText("powershell");

  sendText("$client=new-object System.Net.WebClient");
  sendText("$client.DownloadFile(\"https://wallpaper.dog/large/20380488.jpg\" , \"a.bat\")");

  
  sendText("static extern int SystemParametersInfo(int uAction,int uParam,string lpvParam,int fuWinIni);public static void SetWallpaper");  /*
  sendText("public static void SetWallpaper(string thePath){");

  sendText("SystemParametersInfo(20,0,thePath,3);");
  sendText("}}}");
  sendText("'@");
  sendText("add-type $code");
  sendText("[Win32.Wallpaper]::SetWallpaper($imgPath)");
  */
  sendText("exit");
  // DigiKeyboard.sendKeyStroke(KEY_D, MOD_GUI_LEFT);
  DigiKeyboard.delay(500);
  for(;;){}
}
