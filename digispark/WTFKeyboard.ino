/// # WTFKeyboard
/// Minimize all window or lock windows;
/// Random timer from 50min to 2h
///
// For en-US Keyboard remove comment on line bellow
#include "DigiKeyboard.h"
long r;

void setup() {
  // put your setup code here, to run once:
  DigiKeyboard.sendKeyStroke(0);
  randomSeed(analogRead(0));
}

void loop() {
  r = random(5, 10);
  delay(r * 60 * 1000);  // Roda a cada 20min até 2h
  if(r%2 == 0) {
    DigiKeyboard.sendKeyStroke(KEY_L, MOD_GUI_LEFT);
  } else {
    DigiKeyboard.sendKeyStroke(KEY_D, MOD_GUI_LEFT);
  }
}
