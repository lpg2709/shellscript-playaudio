# Digispark scripts

Folder for Digispark scripts, using Arduino plataform

## Dev Env

For configure Installation Instructions see: [http://digistump.com/wiki/digispark/tutorials/connecting](http://digistump.com/wiki/digispark/tutorials/connecting)


### Use ABNT2 keyboards

For use ABNT2, move the files on `./ABNT2-lib` to:

- Windows:

```
%LOCALAPPDATA%\Arduino15\packages\digistump\hardware\avr\1.6.7\libraries\DigisparkKeyboard
```

## Scripts

- *Set Wallpaper Digispark [ Only for windows ]*

Change wallpeper from windows using Digispark like HID

